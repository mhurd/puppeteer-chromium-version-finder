const fetch = require('cross-fetch');
const fs = require('fs');
const path = require('path');

const getPuppeteer = () => {
  try {
    return require('puppeteer');
  } catch (error) {
    try {
      return require('puppeteer-core');
    } catch (error) {
      throw new Error('Unable to find puppeteer or puppeteer-core in the current context. This package does not install either.');
    }
  }
};

const getPuppeteerChromiumRevision = (module) => {
  return (
    module.default?.defaultBrowserRevision ??
    module.default?._preferredRevision ??
    module._preferredRevision ??
    module._launcher?._preferredRevision
  );
};

function isVersion(possibleVersion) {
  return /^\d+\.\d+\.\d+\.\d+$/.test(possibleVersion);
}

const getPuppeteerChromiumVersion = async () => {
  const puppeteer = getPuppeteer();
  const chromeRevision = getPuppeteerChromiumRevision(puppeteer);

  if (!chromeRevision) {
    throw new Error('Unable to find chrome revision');
  }

  if (isVersion(chromeRevision)) {
    const versionParts = chromeRevision.split('.');
    return {
      MAJOR: versionParts[0],
      MINOR: versionParts[1],
      BUILD: versionParts[2],
      PATCH: versionParts[3]
    };
  }

  const revisionResponse = await fetch(`http://crrev.com/${chromeRevision}`);
  const versionResponse = await fetch(`${revisionResponse.url}/chrome/VERSION?format=TEXT`);
  const versionBase64 = await versionResponse.text();
  const chromeVersionString = Buffer.from(versionBase64, 'base64').toString('utf-8');

  return chromeVersionString.split('\n').reduce((chromeVersion, line) => {
    const [key, value] = line.split('=');
    if (key && value) {
      chromeVersion[key] = parseInt(value, 10);
    }
    return chromeVersion;
  }, {});
};

module.exports = { getPuppeteerChromiumVersion };
