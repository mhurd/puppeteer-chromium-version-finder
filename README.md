Provides a method for getting the Chromium version expected by Puppeteer

See `index.test.js` for examples

You can also run `make test` or `make sample` to run the tests or a sample call in a Docker container
