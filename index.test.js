const { exec } = require('child_process');
const util = require('util');
const execAsync = util.promisify(exec);
const temp = require('temp').track();
const fs = require('fs-extra');

const { getPuppeteerChromiumVersion } = require('./index');

jest.setTimeout(60000);
jest.retryTimes(3);

const packages = [
  'puppeteer@21.5.0',
  'puppeteer@21.4.0',
  'puppeteer@21.3.7',
  'puppeteer@21.3.2',
  'puppeteer@21.3.0',
  'puppeteer@21.1.0',
  'puppeteer@21.0.2',
  'puppeteer@21.0.0',
  'puppeteer@20.9.0',
  'puppeteer@20.7.2',
  'puppeteer@20.6.0',
  'puppeteer@20.1.0',
  'puppeteer@20.0.0',
  'puppeteer@19.8.0',
  'puppeteer@19.7.0',
  'puppeteer@19.6.0',
  'puppeteer@19.4.0',
  'puppeteer@19.2.0',
  'puppeteer@18.1.0',
  'puppeteer@17.1.0',
  'puppeteer@15.5.0',
  'puppeteer@15.1.0',
  'puppeteer@14.2.0',
  'puppeteer@14.0.0',
  'puppeteer@13.6.0',
  'puppeteer@13.5.0',
  'puppeteer@13.2.0',
  'puppeteer@13.1.0',
  'puppeteer@12.0.0',
  'puppeteer@10.2.0',
  'puppeteer@10.0.0',
  'puppeteer@9.0.0',
  'puppeteer@8.0.0',
  'puppeteer@7.0.0',
  'puppeteer@6.0.0',
  'puppeteer@5.5.0',
  'puppeteer@5.4.0',
  'puppeteer@5.3.0',
  'puppeteer@5.2.1',
  'puppeteer@5.1.0',
  'puppeteer@3.1.0',
  'puppeteer@3.0.0',
  'puppeteer@2.1.0',
  'puppeteer@2.0.0',
  'puppeteer@1.20.0',
  'puppeteer@1.19.0',
  'puppeteer@1.17.0',
  'puppeteer@1.15.0',
  'puppeteer@1.13.0',
  'puppeteer@1.12.2',
  'puppeteer-core@18.2.0',
  'puppeteer@15.2.0',
  'backstopjs@4.5.1',
  'backstopjs@6.1.1',
  'backstopjs@6.1.2',
  'backstopjs@6.1.3',
  'backstopjs@6.1.4',
  'backstopjs@6.2.0',
  'backstopjs@6.2.1',
  'webdriverio@7.16.13',
  'webdriverio@8.9.0'
];

// docs: https://pptr.dev/chromium-support
const expectedChromiumVersions = [
  '119.0.6045.105',
  '118.0.5993.70',
  '117.0.5938.149',
  '117.0.5938.92',
  '117.0.5938.88', // docs say 62
  '116.0.5845.96',
  '115.0.5790.170',
  '115.0.5790.102',
  '115.0.5790.98',
  '114.0.5735.133',
  '114.0.5735.90',
  '113.0.5672.63',
  '112.0.5615.121',
  '112.0.5614.0',
  '111.0.5555.0', // docs say 5556
  '110.0.5478.0', // docs say 5479
  '109.0.5412.0',
  '108.0.5351.0',
  '107.0.5296.0',
  '106.0.5249.0',
  '105.0.5173.0',
  '104.0.5109.0',
  '103.0.5058.0', // docs say 5059
  '102.0.5002.0',
  '101.0.4950.0',
  '100.0.4889.0',
  '99.0.4844.0', // docs say 16
  '98.0.4758.0',
  '97.0.4691.0', // docs say 4692
  '93.0.4577.0',
  '92.0.4512.0',
  '91.0.4469.0',
  '90.0.4427.0',
  '90.0.4403.0',
  '89.0.4389.0',
  '88.0.4298.0',
  '87.0.4272.0',
  '86.0.4240.0',
  '85.0.4182.0',
  '84.0.4147.0',
  '83.0.4103.0',
  '81.0.4044.0',
  '80.0.3987.0',
  '79.0.3945.0', // docs say 3942
  '78.0.3882.0',
  '77.0.3844.0', // docs say 3803
  '76.0.3803.0',
  '75.0.3765.0',
  '74.0.3723.0',
  '73.0.3679.0',
  '107.0.5296.0',
  '104.0.5109.0',
  '80.0.3987.0',
  '105.0.5173.0',
  '105.0.5173.0',
  '105.0.5173.0',
  '105.0.5173.0',
  '105.0.5173.0',
  '112.0.5614.0',
  '101.0.4950.0',
  '112.0.5614.0'
];

async function setupTestEnvironment(packageName, version) {
  const tempDir = temp.mkdirSync('test-environment');
  await fs.copy('./', tempDir, {
    filter: (src) => {
      return !src.includes('node_modules');
    }
  });
  await execAsync(`PUPPETEER_SKIP_DOWNLOAD=true PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true npm install ${packageName}@${version}`, { cwd: tempDir });
  return tempDir;
}

function cleanupTestEnvironment(tempDir) {
  temp.cleanupSync();
}

function getNameVersionAndExpectedVersionForPackage(pkg, index) {
  return [...pkg.split('@'), expectedChromiumVersions[index]];
}

describe.each(packages.map(getNameVersionAndExpectedVersionForPackage))('%s', (packageName, packageVersion, chromiumVersion) => {
  let testEnvironmentPath;

  beforeAll(async () => {
    testEnvironmentPath = await setupTestEnvironment(packageName, packageVersion);
  });

  afterAll(() => {
    cleanupTestEnvironment(testEnvironmentPath);
  });

  test(`Testing with ${packageName} version ${packageVersion} (expected Chromium version: ${chromiumVersion})`, async () => {
    const getPuppeteerChromiumVersionInEnv = require(`${testEnvironmentPath}/index`).getPuppeteerChromiumVersion;
    const chromiumVersionObj = await getPuppeteerChromiumVersionInEnv();
    const chromiumVersionString = `${chromiumVersionObj.MAJOR}.${chromiumVersionObj.MINOR}.${chromiumVersionObj.BUILD}.${chromiumVersionObj.PATCH}`;
    expect(chromiumVersionString).toEqual(chromiumVersion);
  }, 60000);
});
