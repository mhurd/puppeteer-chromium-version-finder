.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	-make stop
	-make remove
	make prepare

.PHONY: prepare
prepare:
	docker build -t puppeteer-chromium-version-finder .

.PHONY: remove
remove:
	docker rm puppeteer-chromium-version-finder

.PHONY: stop
stop:
	docker stop puppeteer-chromium-version-finder

.PHONY: test
test:
	make
	docker run -it --rm puppeteer-chromium-version-finder /bin/sh -c "npm test"

.PHONY: sample
sample:
	make
	docker run -it --rm puppeteer-chromium-version-finder /bin/sh -c "\
		npm install puppeteer-core && \
		echo && \
		echo 'Puppeteer version' && \
		npm list puppeteer-core && \
		echo 'Chromium version' && \
		node -e '(async () => { \
			console.log(await require(\"./index.js\").getPuppeteerChromiumVersion()); \
		})()' && \
		echo \
	"
